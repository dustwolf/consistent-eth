# Consistent ETH

Ansible role for disabling consistent network interface names

## Please note

I ultimately abandoned this role because it seems interface persistence doesn't work in CentOS 8-stream. This might be fine for VMs which always boot up the same way, but for physical machines it could result in interface names coming up in the wrong order.
